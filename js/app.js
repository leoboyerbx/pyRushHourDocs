/* ---- Effect "ripple" sur les boutons -------- */
var setMaterialButtons = function () {
    var buttons = document.querySelectorAll('.materialbutton')
    Array.prototype.forEach.call(buttons, function (b) {
        b.addEventListener('click', createRipple);
    });

    function createRipple(e) {
        var circle = document.createElement('div');
        this.appendChild(circle);

        var d = Math.max(this.clientWidth, this.clientHeight);

        circle.style.width = circle.style.height = d + 'px';

        var rect = this.getBoundingClientRect();
        circle.style.left = e.clientX - rect.left - d / 2 - 10 + 'px';
        circle.style.top = e.clientY - rect.top - d / 2 + 'px';

        circle.classList.add('ripple');
        window.setTimeout(function () {circle.parentNode.removeChild(circle)}, 600)
    }
}








/* ---- Animation du menu -------- */

window.onscroll = function () {
    var mainNav = document.querySelector('#main-nav');
    var mainPage = document.querySelector('#main-page')
    var offsetScroll = mainPage.offsetTop - (mainNav.offsetTop + mainNav.offsetHeight);
    var secondHead = document.querySelector('#secondary-header')
    var mainHead = document.querySelector('#main-header')
    var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
    if (scrollTop <= offsetScroll) {
        mainNav.classList.remove('hidden');
        closeMenu();
    } else if (scrollTop > offsetScroll && scrollTop < 200) {
        mainNav.classList.add('hidden');
        secondHead.classList.remove('visible')
        mainHead.classList.remove('hidden')
    }
    else {
        secondHead.classList.add('visible')
        mainHead.classList.add('hidden')
        mainNav.classList.add('hidden');
    }
}



/* ---- Menu latéral -------- */

var menubutton = document.querySelector('#menubutton');
var toggleMenu = function () {
    var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if (w <= 750) {
        window.scrollTo(0, 0);
    }
  document.querySelector('#main-page').classList.toggle('menuopen');
  document.querySelector('#main-nav').classList.toggle('menuopen');
  document.querySelector('#main-header').classList.toggle('menuopen');
  document.querySelector('#detailed-menu').classList.toggle('open');
  menubutton.classList.toggle('open');
   
}
var closeMenu = function () {
  document.querySelector('#main-page').classList.remove('menuopen');
  document.querySelector('#main-nav').classList.remove('menuopen');
  document.querySelector('#main-header').classList.remove('menuopen');
  document.querySelector('#detailed-menu').classList.remove('open');
  menubutton.classList.remove('open');
}

menubutton.addEventListener('click', toggleMenu);



var toggleSubMenu = function (li) {
    li.querySelector('.dm-sub-menu').classList.toggle('open');
}

