var loading = function(action) {
  if (action == "start") {
    document.querySelector('#loader').classList.add('open')

  } else {
    document.querySelector('#loader').classList.remove('open')
  }
}

var hashchange;

var openPage = function(page, hash) {
  var xhr = new XMLHttpRequest()

  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        var content
        var indexDataStart = xhr.responseText.indexOf('<!--DataStart-->')
        var indexDataEnd = xhr.responseText.indexOf('<!--DataEnd-->');
        if (indexDataStart != -1 && indexDataEnd != -1) {
          var lenghtToGet = indexDataEnd - indexDataStart - 16
          content = xhr.responseText.substr(indexDataStart + 16, lenghtToGet)
          if (page == './pages/introduction.html') {
            document.location.hash = ""
          } else {
            document.location.hash = page
          }
        } else {
          content = "Erreur de connexion."
        }
        document.querySelector('#main-content').innerHTML = content
        document.querySelectorAll("pre, code").forEach(function(block) {hljs.highlightBlock(block);})
        if (xhr.status === 200) {
          var title = document.querySelector('#titrepage').innerHTML
          if (title) {
            document.title = 'ISN - ' + title
          } else {
            document.title = 'ISN - La supranationalité des réseaux'
          }
        }

        if (hash) {
          var element = document.getElementById(hash)
          if (element) {
            window.scrollTo(0, element.offsetTop + 100)
          } else {
            window.scrollTo(0, 0)
          }
        }

      } else {
        alert('Erreur 404: document non trouvé.')
      }
      loading('stop')
    }
  }

  xhr.open('GET', page, true)
  xhr.send()
  loading('start')
}

var autoPage = function() {
  var hash = document.location.hash
  if (!hash) {
    openPage('./pages/introduction.html', false)
  } else if (hash != './pages/introduction.html') {
      path = hash.substring(1)
      openPage(path, false)
    }
  }


window.onload = function() {
  setMaterialButtons()
  autoPage()
}

window.onpopstate = autoPage